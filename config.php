<?php

return [
    [
        'name' => 'carousel',
        'title' => '轮播图分类',
        'type' => 'array',
        'content' => [
            'home' => ' ',
        ],
        'value' => [
            'home' => '首页轮播图',
        ],
        'rule' => 'required',
        'msg' => '',
        'tip' => '',
        'ok' => '',
        'extend' => '',
    ],
    [
        'name' => 'tab',
        'title' => '标签导航分类',
        'type' => 'array',
        'content' => [
            'home' => ' ',
        ],
        'value' => [
            'home' => '首页导航',
            'post' => '文章列表导航',
            'example' => '案例导航',
        ],
        'rule' => 'required',
        'msg' => '',
        'tip' => '',
        'ok' => '',
        'extend' => '',
    ],
    [
        'name' => 'typeList',
        'title' => '分类类型',
        'type' => 'array',
        'content' => [
            'menu' => ' ',
        ],
        'value' => [
            'menu' => '菜谱',
            'product' => '商品',
        ],
        'rule' => 'required',
        'msg' => '',
        'tip' => '',
        'ok' => '',
        'extend' => '',
    ],
];
